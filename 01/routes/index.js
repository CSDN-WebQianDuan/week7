var express = require('express');
var router = express.Router(); // 路由对象

/* GET home page. */
// router 是mini app
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

module.exports = router;
