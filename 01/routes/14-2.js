const express = require("express");
const router = express.Router();
const multer = require("multer");
const fs = require("fs");
const upload = multer({
  dest:"upload"
});

const single = upload.single("myfile");

const arr = [];

router.get("/",function (req,res) {
  // res.render("14",{list:arr});
  res.locals.list= arr;
  res.render("14");
});

// <img src="../img/xxxx" >
router.get("/img/:imgname",function (req,res) {
   // 图片文件数据的［读取流］
   const rs = fs.createReadStream("upload/"+req.params.imgname);
   rs.pipe(res);
})

router.post("/up",single,function (req,res) {
   arr.push(req.file);
   res.redirect("back");
})

router.post("/xhrup",single,function (req,res) {
   res.send(req.file);
})


module.exports = router;
