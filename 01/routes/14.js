const express = require("express");
const router = express.Router();
const multer = require("multer");
const upload = multer({
  dest:"upload"
});

const single = upload.single("myfile");
const arr = upload.array("myfiles",3);
const fields = upload.fields([
  {name:"f1",maxCount:1},
  {name:"f2",maxCount:3}
]);

router.post("/up1",single,function (req,res) {
   res.send(req.file);
});

router.post("/up2",arr,function (req,res) {
  res.send(req.files);
})

router.post("/up3",fields,function (req,res) {

  // res.send(req.files);
  res.send(req.body);
})

module.exports = router;
