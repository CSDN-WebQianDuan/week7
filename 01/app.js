var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var sassMiddleware = require('node-sass-middleware');

var index = require('./routes/index');
var users = require('./routes/users');
const l07 = require("./routes/07");
const l09 = require("./routes/09");
const l10 = require("./routes/10");
const l11 = require("./routes/11");
const l12 = require("./routes/12");
var app = express(); // server app instance.

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

// req -> mymymy -> oga -> logger -> body -> cookie -> sass -> res
// app.use(function mymymy (req,res,next) {
//   console.log("mmmmmmm yyyyy ");
//   next();
// });
//
// app.use(function oga(req,res,next){
//   console.log("ooo ggg aaa");
//   next();
// });

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(sassMiddleware({
  src: path.join(__dirname, 'public'),
  dest: path.join(__dirname, 'public'),
  indentedSyntax: true, // true = .sass and false = .scss
  sourceMap: true
}));

app.use(express.static(path.join(__dirname, 'public')));

// get / post
app.get("/test",function (req,res) {
  // logic
  res.send("test tsetestsetsetsts! get get !");
  // res.send("dddd"); xxxxx error!
});

// req -> use -> post -> res
app.post("/test",function (req,res) {
  res.send("test tsetestsetsetsts! post post ");
});


app.use('/', index);
app.use('/users2', users);
app.use("/lesson07", l07);
app.use("/lesson09", l09);
app.use("/lesson10", l10);
app.use("/lesson11", l11);
app.use("/lesson12", l12);
app.use("/lesson14", require("./routes/14"));
app.use("/lesson14-2",require("./routes/14-2"));
app.use("/lesson15", require("./routes/15"));
app.use("/lesson16", require("./routes/16"));
app.use("/lesson18", require("./routes/18"));
app.use("/lesson19", require("./routes/19"));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
