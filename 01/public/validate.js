function validate(name,email){
  let errors = "";
  if(!(name && name.length > 2 && name.length < 10)){
    errors = errors || {};
    errors.name = "name length must > 2 and < 10";
  }
  if(!(/^[a-zA-Z]\w+@[a-zA-Z]\w+\.[a-zA-Z]\w+(\.[a-zA-Z]\w+)?$/.test(email))){
    errors = errors || {};
    errors.email = "你的email 格式有问题";
  }
  return errors;
}

if(typeof window === "undefined")
  module.exports = validate;
